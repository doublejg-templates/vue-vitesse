FROM node:16 as install

USER node

WORKDIR /app

COPY --chown=node:node package.json ./
COPY --chown=node:node yarn.lock ./

RUN yarn

FROM node:16 as build

USER node

WORKDIR /app

COPY --from=install /app ./
COPY . . 

RUN yarn build


FROM node:16 as dev
USER node
WORKDIR /app

COPY --from=install --chown=node:node /app ./
COPY --chown=node:node . . 

EXPOSE 3333

CMD ["yarn", "dev:headless"]
