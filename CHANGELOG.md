# [1.1.0](https://gitlab.com/doublejg-templates/vue-vitesse/compare/v1.0.0...v1.1.0) (2022-06-19)


### Bug Fixes

* **ci:** install dependencies needed in ci pipelines ([9e9c8c6](https://gitlab.com/doublejg-templates/vue-vitesse/commit/9e9c8c65bd21054836171692e878203852185511))


### Features

* semantic-release/git ([0050dd9](https://gitlab.com/doublejg-templates/vue-vitesse/commit/0050dd988e603ce2b37fb8fb7f399780452b4cb6))
